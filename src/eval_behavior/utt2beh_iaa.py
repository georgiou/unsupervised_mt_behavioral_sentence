from __future__ import print_function

import sys
import os
import glob
import pickle

import numpy as np

import pathos.pools as pp
import krippendorff_alpha as kalpha

import time
import GPUtil

# =============================================================================================================================
# Pool function
# =============================================================================================================================

def test_coup_id(coup_id) :

   # np.random.seed()
   os.environ["CUDA_VISIBLE_DEVICES"] = '-1'

   import tensorflow as tf
   
   import keras
   from keras.models import Sequential
   from keras.layers import Dense, LSTM, Dropout, Activation
   from keras.callbacks import ModelCheckpoint
   from keras import backend as k

   from sklearn.svm import SVR

   
   # TensorFlow wizardry
   config = tf.ConfigProto()
   
   # Don't pre-allocate memory; allocate as-needed
   config.gpu_options.allow_growth = True
   
   # Only allow a total of half the GPU memory to be allocated
   # config.gpu_options.per_process_gpu_memory_fraction = 0.5
   
   # Create a session with the above options specified.
   k.tensorflow_backend.set_session(tf.Session(config=config))

   print('Processing data split')
   with open(pklpath,'rb') as fh_pkl :
      dset = pickle.load(fh_pkl)
   X = np.array([ d for i,d in enumerate(dset['data']) if coup_id != dset['sess'][i].split('.')[0] ])
   y = np.array([ d[0] for i,d in enumerate(dset['label']) if coup_id != dset['sess'][i].split('.')[0] ])
   tr_sess_id = [ dset['sess'][i] for i in range(len(dset['label'])) if coup_id != dset['sess'][i].split('.')[0] ]

   X_ts = np.array([ d for i,d in enumerate(dset['data'])  if coup_id == dset['sess'][i].split('.')[0] ])
   y_ts = np.array([ d[0] for i,d in enumerate(dset['label']) if coup_id == dset['sess'][i].split('.')[0] ])
   ts_sess_id = [ dset['sess'][i] for i in range(len(dset['label'])) if coup_id == dset['sess'][i].split('.')[0] ]
   
   
   embed_size = X[0][0].shape[0]
   
   print('Build model')
   model = Sequential()
   model.add(LSTM(hidden_size, input_shape=(3, embed_size), return_sequences=True))
   model.add(LSTM(hidden_size, return_sequences=False))
   model.add(Dense(1))
   model.add(Activation('sigmoid'))
   
   model.compile(loss='mean_squared_error',
           optimizer='adagrad',
           metrics=['mse'])

   weights = '{}/{}_model_sigmoid.h5'.format(weightdir, coup_id)
   model.load_weights(weights)

   print('Start testing')
   sess_dict = {}
   sess_dict_ts = {}
   
   y_pred = model.predict(x=X, batch_size=256) 
            
   for i,v in enumerate(y_pred) :
      sess_id = tr_sess_id[i]
      if sess_id not in sess_dict :
         sess_dict[sess_id] = {}
         sess_dict[sess_id]['pred'] = []
         sess_dict[sess_id]['rating'] = y[i]
      
      sess_dict[sess_id]['pred'].append(v)

   X_svr = np.array([ np.median(sess_dict[x]['pred']) for x in sess_dict ])
   y_svr = np.array([ sess_dict[x]['rating'] for x in sess_dict ])
   y_svr = (y_svr - 1)/8

   svr_rbf = SVR(kernel='rbf', C=1, gamma=1, shrinking=False)
   svr_rbf.fit(X_svr.reshape(-1,1), y_svr)

   # Apply SVR on test set
   y_pred = model.predict(x=X_ts, batch_size=256) 
   for i,v in enumerate(y_pred) :
     sess_id = ts_sess_id[i]
     if sess_id not in sess_dict_ts :
        sess_dict_ts[sess_id] = {}
        sess_dict_ts[sess_id]['pred'] = []
        sess_dict_ts[sess_id]['rating'] = y_ts[i]
     
     sess_dict_ts[sess_id]['pred'].append(v)
  
   for sess_id in sess_dict_ts : 
      sess_dict_ts[sess_id]['svr'] = np.round(svr_rbf.predict(np.median(sess_dict_ts[sess_id]['pred']).reshape(1,-1))[0] * 8 + 1)
   
   print('Done for {}'.format(coup_id))
   return sess_dict_ts


# =============================================================================================================================
# Main Code
# =============================================================================================================================
if __name__ == '__main__':
   code = 'negative'
   behaviors = ['negative']
   
   hidden_size = 10
   use_dropout = True

   if len(sys.argv) == 2 : 
       embed_dir = sys.argv[1]
   elif len(sys.argv) == 3 : 
       embed_dir = sys.argv[1]
       code = sys.argv[2]
       behaviors = [code]
   elif len(sys.argv) == 4 : 
       embed_dir = '../train_seq2seq/experiments/{}/embeddings/{}_{}'
       embed_dir = embed_dir.format(sys.argv[1], sys.argv[2], sys.argv[3])
   elif len(sys.argv) == 5 : 
       embed_dir = '../train_seq2seq/experiments/{}/embeddings/{}_{}'
       embed_dir = embed_dir.format(sys.argv[1], sys.argv[2], sys.argv[3])
       code = sys.argv[4]
       behaviors = [code]
   
   
   lists_mod = './lists_mod/{}.txt'.format(code)
   print(embed_dir)

   outdir = os.path.join(embed_dir, 'utt2beh', code)
   pklpath = os.path.join(outdir, 'data.pkl')

   weightdir = os.path.join(outdir, 'weights')
   # weightdir = os.path.join('/tmp/2334629.main', 'weights')
   if not os.path.exists(weightdir) :
      print('Weight directory not find')
      sys.exit(1)

   print('Searching for models')
   couple_ids = np.unique([ x.split('/')[-1].split('_')[0] for x in glob.glob(os.path.join(weightdir, '*.h5'))])
   print('\t{} models found'.format(len(couple_ids)))

   print('Start leave-one-couple-out IAA')
   pool = pp.ProcessPool(nodes=6)
   res = {}

   for r in pool.amap(test_coup_id, couple_ids).get() :
      res.update(r)

   
   human_rating = dict( (sess_id, res[sess_id]['rating']) for sess_id in res)
   machine_rating = dict( (sess_id, res[sess_id]['svr']) for sess_id in res)
   
   def ordinal_metric(x,y) :
      if x==y : return 0
      m, t = sorted((x,y))
      N = np.sum( np.arange(m,t+1))
      return (N - np.mean([x,y]))**2

   with open('../prep_data/individual_annotators_{}.pkl'.format(code), 'rb') as fh :    
       coders = pickle.load(fh)

   data = [ coders[x] for x in coders ]
   
   iaa = kalpha.krippendorff_alpha(data, metric=ordinal_metric)
   # iaa = krippendorff.alpha(data, level_of_measurement='ordinal')
   # # iaa = 0.8286035042920031
   print('Human inter-annotator agreement : {}'.format(iaa))

   random_drop = 3

   def krip_alpha(random_drop) :
       with open('../prep_data/individual_annotators_{}.pkl'.format(code), 'rb') as fh :    
           coders = pickle.load(fh)

       import numpy 
       numpy.random.seed()
       data = [ coders[x] for x in coders ] 
       
       annotators = coders.keys()

       for sess in machine_rating : 
           data[numpy.random.randint(len(data))][sess] = machine_rating[sess]
           
       
       # Machine as random annotator
       # data = [ machine_rating if i == random_drop else coders[x] for i, x in enumerate(coders)]

       return kalpha.krippendorff_alpha(data, metric=ordinal_metric)

   pool = pp.ProcessPool(nodes=20)
   res = pool.amap(krip_alpha, range(10))

   results = []
   for iaa in res.get() :
       print('Random inter-annotator agreement : {}'.format(iaa))
       results.append(iaa)

   print('Average : {:.3f}'.format(np.mean(results)))

   coder_stats = {} # Average error of coder
   for coder in coders :
       coder_stats[coder] = []
       for sess_name in coders[coder] : 
           if sess_name in human_rating : 
               coder_stats[coder].append( abs(coders[coder][sess_name] - human_rating[sess_name]))
       coder_stats[coder] = np.mean(coder_stats[coder])

   worst_coder = max(coder_stats, key=coder_stats.get)
   print('Worst annotator is {} (MAE {:.3f})'.format(worst_coder, coder_stats[worst_coder]))
   data = [ machine_rating if coder == worst_coder else coders[coder] for coder in coders ]
   iaa = kalpha.krippendorff_alpha(data, metric=ordinal_metric)
   print('Worst-annotator-out IAA : {:.3f}'.format(iaa))

   import copy
   worst_rating_out = copy.deepcopy(coders)
   for sess_name in machine_rating :
       # Distance of each annotation to mean value
       sess_rating = dict([ (x,abs(human_rating[sess_name]-coders[x][sess_name])) for x in coders if sess_name in coders[x] ])
       # Worst annotation amongst coders
       coder_worst_annotation = max(sess_rating, key=sess_rating.get)
       # Replace that annotation with machine
       worst_rating_out[coder_worst_annotation][sess_name] = machine_rating[sess_name]

   data = [ worst_rating_out[coder] for coder in worst_rating_out]
   iaa = kalpha.krippendorff_alpha(data, metric=ordinal_metric)
   print('Worst-annotation-out IAA : {}'.format(iaa))

