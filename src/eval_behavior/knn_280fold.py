
from __future__ import print_function

import pathos.pools as pp

import os, sys
import numpy as np
from numpy import linalg as LA
import random

# import scipy.cluster.vq as vq

from utils import movingAverage, geometricMedian

 #import pathos.pools as pp
# from pathos.pools import ProcessPool as Pool

# =============================================================================================================================
# K-NN Function
# =============================================================================================================================
def f_knn(test_list, n_pca=50, n_components=2, use_lda=False, use_pca=True) : 
    '''
        Returns :
            couples_data for test couple_id
    '''
    import numpy as np
    from sklearn.decomposition import PCA
    from sklearn.neighbors import KNeighborsClassifier

    np.random.seed()

    # couples_data , test_list = args
    assert test_list[0] in couples_data, ','.join([str(i) for i in couples_data.keys()])

    # =============================================================================================================================
    # Split data into train/val/test
    # =============================================================================================================================
    print('Leave {} out'.format(','.join([str(x) for x in test_list])))
    val_id = list(couples_data.keys())[np.random.randint(len(couples_data))]
    while val_id in test_list :
        val_id = list(couples_data.keys())[np.random.randint(len(couples_data))]
    print('Validation on {}'.format(val_id))

    X = []
    y = []
    couple_score = []
    couple_idx = []
    cur_offset = 0
    for coup_id in couples_data : 
        if coup_id in test_list or coup_id == val_id: 
            continue

        for sess_id in couples_data[coup_id] : 
            embeddings = couples_data[coup_id][sess_id]['embeddings']
            score = couples_data[coup_id][sess_id]['score']
            couple_size = len(embeddings)
            X.extend( embeddings)
            y.extend(  [score] * couple_size )
            couple_score.append(score)
            couple_idx.append(range(cur_offset, couple_size + cur_offset))
            cur_offset += couple_size
            
        
    assert len(X) == len(y)
    X = np.array(X)

    if use_lda : 
        print('Running LDA')
        clf = LDA()
        clf.fit(X, y)
    
    if use_pca : 
        print('Running PCA-{}'.format(n_pca))
        pca = PCA(n_components=n_pca)
        X = pca.fit_transform(X)
    
    neigh = KNeighborsClassifier(n_neighbors=6, n_jobs=2, weights='distance')
    neigh.fit(X,y)

    # means, y = vq.knn2(X, n_components)
    val_acc = []
    for sess_id in couples_data[val_id] : 
        embeddings = couples_data[val_id][sess_id]['embeddings']
        true_score = couples_data[val_id][sess_id]['score']

        X = pca.transform(embeddings)

        pred_knn = neigh.predict(X)

        val_acc.append(true_score == (np.mean(pred_knn) > 0.5))

    val_acc = np.mean(val_acc)

    ret = {}
    for test_id in test_list : 
        ret[test_id] = {}
        for sess_id in couples_data[test_id] : 
            embeddings = couples_data[test_id][sess_id]['embeddings']
            true_score = couples_data[test_id][sess_id]['score']

            X = pca.transform(embeddings)

            pred_knn = neigh.predict(X)

            ret[test_id][sess_id] = {}
            ret[test_id][sess_id]['score'] = np.mean(pred_knn)
            ret[test_id][sess_id]['val_acc'] = val_acc

    print('Done for {}'.format(','.join([str(x) for x in test_list])))
    return ret
    
# =============================================================================================================================
# Main Code
# =============================================================================================================================
if __name__ == '__main__':
    if len(sys.argv) == 2 : 
        embed_dir = sys.argv[1]
        code = 'negative'
    elif len(sys.argv) == 3 : 
        embed_dir = sys.argv[1]
        code = sys.argv[2]
    elif len(sys.argv) == 4 : 
        embed_dir = '../train_seq2seq/experiments/{}/embeddings/{}_{}'
        embed_dir = embed_dir.format(sys.argv[1], sys.argv[2], sys.argv[3])
        code = 'negative'
    elif len(sys.argv) == 5 : 
        embed_dir = '../train_seq2seq/experiments/{}/embeddings/{}_{}'
        embed_dir = embed_dir.format(sys.argv[1], sys.argv[2], sys.argv[3])
        code = sys.argv[4]

    behaviors = [code]

    lists_mod = './lists_mod/{}.txt'.format(code)

    print(embed_dir)
    
    beh_pos = dict( [(b, 2*i) for i, b in enumerate(behaviors)])
    
    n_jobs = 30
    n_couples   = 280
    n_components = 2
    random_restart = 1
    normalize_embeddings = True
    use_moving_average = True
    use_pca = True
    n_pca = 50
    use_lda = False
    shuffle = True
    
    timeframes = ['pre', '26wk', '2yr']
    
    # =============================================================================================================================
    # Load embedding data
    # =============================================================================================================================
    coup_ids = []
    fh = open(lists_mod, 'r')
    lines = fh.readlines()
    if shuffle : 
        random.shuffle(lines)
    couple_list = lines[0:n_couples]
    
    print('Loading couples list')
    for line in lines : 
        cols = line.strip().split(',')
        coup_ids.append(cols[-1].split('.')[0])
    coup_ids = np.unique(sorted(coup_ids))
    
    print('Loading data')
    X = [] 
    trans = []
    couple_sizes = []
    couple_idx = []
    couple_score = []
    neg_couple_idx = []
    pos_couple_idx = []
    wif_idx = []
    hus_idx = []
    cur_offset = 0 
    y = dict( [(b,[]) for b in behaviors])
    
    couples_data = {}

    for line in couple_list : 
        # eg. W,0,1.000,465.26wk.ps.h
        cols = line.strip().split(',')
        spk  = 'spk-{}'.format(cols[0].lower())
        try : 
            neg_score = int(cols[beh_pos['negative']])
            neg_rating= float(cols[beh_pos['negative']+1])
        except :
            neg_score = int(cols[1])
            neg_rating= float(cols[2])
    
        couple_filename = '.'.join([cols[-1], spk, 'txt'])
        couple_filepath = os.path.join(embed_dir,couple_filename)
        couple_id = int(cols[-1].split('.')[0])
        couple_timeframe = cols[-1].split('.')[1]
        sess_id = '.'.join(cols[-1].split('.')[1:] + [spk])
        print('({}) {}'.format(neg_score, couple_filename))
    
        if os.path.exists(couple_filepath) :
            couple_embed = np.loadtxt(couple_filepath)
        elif os.path.exists(couple_filepath.replace('.txt','.npy')) :
            couple_filepath = couple_filepath.replace('.txt','.npy')
            couple_embed = np.load(couple_filepath)
        else :
            print("\tERROR: File not found {}".format(couple_filepath))
        
        couple_size = couple_embed.shape[0]
        if couple_size == 0 : 
            print("\tERROR: Empty file {}".format(couple_filepath))
            sys.exit()
    
        if normalize_embeddings : 
            couple_embed = couple_embed / LA.norm(couple_embed, axis=-1).reshape(couple_size,1)
        if use_moving_average :
            couple_embed = movingAverage(couple_embed)
        # Store collected data
        if couple_id not in couples_data :
            couples_data[couple_id] = {}
    
        if sess_id not in couples_data[couple_id] : 
            couples_data[couple_id][sess_id] = {}
    
        couples_data[couple_id][sess_id]['embeddings'] = couple_embed.tolist()
        couples_data[couple_id][sess_id]['score'] = neg_score
        couples_data[couple_id][sess_id]['rating'] = (neg_rating - 1) / 8
    
    
    all_couple_ids = list(couples_data.keys())
    ret = {}

    pool = pp.ProcessPool(nodes=4)
    res = pool.amap(f_knn, [[i] for i in all_couple_ids])

    for r in res.get() : 
        ret.update(r)
    
    correct = []
    val_acc_all = []
    correct_mean_proba = []
    correct_median_proba = []
    abs_error = { 'maj':[]}


    for coup_id in ret.keys() : 
        val_acc = []
        for sess_id in ret[coup_id] : 
            if len(val_acc) == 0 :
                val_acc_all.append(ret[coup_id][sess_id]['val_acc'])
            true_score = couples_data[coup_id][sess_id]['score']
            true_rating = couples_data[coup_id][sess_id]['rating']

            pred_score = ret[coup_id][sess_id]['score']
            abs_error['maj'].append( abs(true_rating - pred_score))
            if true_score == int(pred_score >= 0.5):
                correct.append(1.)
            else : 
                correct.append(0.)


    print('Total Val accuracy : {}'.format(np.mean(val_acc_all)))
    print('Total accuracy : {}'.format(np.mean(correct_mean_proba)))
    
    fh.close()
    
