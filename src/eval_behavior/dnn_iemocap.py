from __future__ import print_function

import os, sys
import numpy as np
from numpy import linalg as LA
import random
import pickle
import tempfile

import tensorflow as tf
from keras import backend as k

import keras
from keras.models import Sequential
from keras.layers import Dropout, Dense, Activation

from sklearn.decomposition import PCA
from sklearn.metrics import f1_score, recall_score, precision_score, accuracy_score

if len(sys.argv) == 2 :
    embed_dir = '../../feats/couples/{}'.format(sys.argv[1])
elif len(sys.argv) == 4 : 
    embed_dir = '../train_seq2seq/experiments/{}/embeddings/{}_{}'
    embed_dir = embed_dir.format(sys.argv[1], sys.argv[2], sys.argv[3])

label_dir = '../../data/iemocap/cv'


all_acc = []
all_wa = []
all_f1s = []
all_prec = []
all_recall = []
val_acc = []

# TensorFlow wizardry
config = tf.ConfigProto()

# Don't pre-allocate memory; allocate as-needed
config.gpu_options.allow_growth = True

# Only allow a total of half the GPU memory to be allocated
# config.gpu_options.per_process_gpu_memory_fraction = 0.5

# Create a session with the above options specified.
k.tensorflow_backend.set_session(tf.Session(config=config))

model_weights = tempfile.mktemp(suffix='.hdf5')
 
for cv in range(1,6) :
    feats_fname = 'cv{}_tr.text.npy'.format(cv)
    label_fname = 'cv{}_tr.txt'.format(cv)
    
    feats_fname_ts = 'cv{}_ts.text.npy'.format(cv)
    label_fname_ts = 'cv{}_ts.txt'.format(cv)
    
    label_names = ['hap', 'sad', 'ang', 'neu']
    label_mapping = dict([(x,i) for i,x in enumerate(label_names)])
    
    feats = np.load(os.path.join(embed_dir, feats_fname))
    feats_ts = np.load(os.path.join(embed_dir, feats_fname_ts))
    
    labels = []
    with open(os.path.join(label_dir, label_fname),'r') as fh :
        for line in fh :
            cols = line.strip().split(' ')
            labels.append(label_mapping[cols[0]])
    
    labels_ts = []
    with open(os.path.join(label_dir, label_fname_ts),'r') as fh :
        for line in fh :
            cols = line.strip().split(' ')
            labels_ts.append(label_mapping[cols[0]])
    
    assert len(labels) == feats.shape[0], 'Labels: {} Feats:{}'.format(len(labels), feats.shape[0])
    assert len(labels_ts) == feats_ts.shape[0]
    
    labels = np.array(labels).reshape(-1,1)
    labels_ts = np.array(labels_ts).reshape(-1,1)
    
    # Pre-shuffle
    idx = np.random.permutation(feats.shape[0])
    feats = feats[idx,:]
    labels = labels[idx,:]
    
    
    # ================================================================================================
    # Classifier model
    # ================================================================================================

    model = Sequential([
        Dense(256, input_shape=(feats.shape[1],)),
        Activation('relu'),
        Dropout(0.2),
        Dense(256),
        Activation('relu'),
        Dropout(0.2),
        Dense(256),
        Activation('relu'),
        Dropout(0.2),
        Dense(256),
        Activation('relu'),
        Dropout(0.2),
        Dense(4),
        Activation('softmax'),
    ])
 
    model.compile(optimizer='adagrad',
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    
   
    callback = keras.callbacks.ModelCheckpoint(model_weights, monitor='val_acc', verbose=0, 
          save_best_only=True, save_weights_only=True, mode='auto', period=1)

    model.fit(feats, keras.utils.to_categorical(labels),
              validation_split=0.1,
              epochs=50,
              callbacks=[callback],
              shuffle=True,
              verbose=0,
              batch_size=32)
    
    model.load_weights(model_weights)
    
    y_pred = model.predict(feats_ts)
    y_pred = np.argmax(y_pred, axis=-1)

    acc = np.mean(y_pred.reshape(-1,1) == labels_ts)
    wa  = accuracy_score(labels_ts, y_pred)
    f1s = f1_score(labels_ts, y_pred, average='weighted')
    prec = precision_score(labels_ts, y_pred, average='weighted')
    recall = recall_score(labels_ts, y_pred, average='weighted')

    all_acc.append(acc)
    all_wa.append(wa)
    all_f1s.append(f1s)
    all_prec.append(prec)
    all_recall.append(recall)

    val_acc.append(callback.best)

    print('CV {} Acc: {:.4f} F1-score: {:.4f}'.format(cv, acc, f1s))

    del model
    del callback

# ================================================================================================
# Evaluation
# ================================================================================================

print('Val Acc {:.4f} ({:.4f}):'.format(np.mean(val_acc), np.std(val_acc)))
print('Avg Acc {:.4f} ({:.4f}):'.format(np.mean(all_acc), np.std(all_acc)))
print('Avg W-Acc {:.4f} ({:.4f}):'.format(np.mean(all_wa), np.std(all_wa)))
print('Avg F1  {:.4f} ({:.4f}):'.format(np.mean(all_f1s), np.std(all_f1s)))
print('Avg WAP  {:.4f} ({:.4f}):'.format(np.mean(all_prec), np.std(all_prec)))
print('Avg WAR  {:.4f} ({:.4f}):'.format(np.mean(all_recall), np.std(all_recall)))

