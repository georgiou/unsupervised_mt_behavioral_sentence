
from __future__ import print_function

import pathos.pools as pp

import os, sys
from time import time
import numpy as np
from numpy import linalg as LA
import random

from utils import movingAverage, geometricMedian


# =============================================================================================================================
# K-Means Function
# =============================================================================================================================
def f_kmeans(args, n_pca=50, n_components=2, use_lda=False, use_pca=False) : 
    '''
        Returns :
            couples_data for test couple_id
    '''
    import numpy as np
    from sklearn.decomposition import PCA
    from sklearn.cluster import KMeans
    from scipy.spatial.distance import euclidean

    np.random.seed()

    # =============================================================================================================================
    # Split data into train/val/test
    # =============================================================================================================================
    couples_data, test_list = args
    print('Leave {} out'.format(','.join([str(x) for x in test_list])))

    val_id = list(couples_data.keys())[np.random.randint(len(couples_data))]
    while val_id in test_list :
        val_id = list(couples_data.key())[np.random.randint(len(couples_data))]
    print('Validation on {}'.format(val_id))

    X = []
    y = []
    couple_score = []
    couple_idx = []
    cur_offset = 0
    for coup_id in couples_data : 
        if coup_id in test_list or coup_id == val_id: 
            continue

        for sess_id in couples_data[coup_id] : 
            embeddings = couples_data[coup_id][sess_id]['embeddings']
            score = couples_data[coup_id][sess_id]['score']
            couple_size = len(embeddings)
            X.extend( embeddings)
            y.extend(  [score] * couple_size )
            couple_score.append(score)
            couple_idx.append(range(cur_offset, couple_size + cur_offset))
            cur_offset += couple_size
            
        
    assert len(X) == len(y)
    X = np.array(X)

    if use_lda : 
        print('Running LDA')
        clf = LDA()
        clf.fit(X, y)
    
    if use_pca : 
        print('Running PCA-{}'.format(n_pca))
        pca = PCA(n_components=n_pca)
        X = pca.fit_transform(X)
    
    kmeans = KMeans(n_clusters=n_components, n_init=50, random_state=None, max_iter=500, n_jobs=2).fit(X)
    y = kmeans.labels_
    cent1, cent2 = kmeans.cluster_centers_[0:2]
    
    dominant = 0 
    all_acc = {a:[] for a in ['Mean','Maj ','Med ']}
    for i in range(n_components) : 
        correct_gmedian = []
        correct_gmean = []
        correct_maj = []
        for score, idx in zip(couple_score, couple_idx) : 
            perc = np.mean( y[idx] == i)
    
            gmean = np.mean(X[idx,:],axis=0)
            gmean_dist1 = euclidean(cent1, gmean)
            gmean_dist2 = euclidean(cent2, gmean)
    
    
            if i == 0 : 
                pred_gmean = np.argmax([gmean_dist1, gmean_dist2])
            elif i == 1 :
                pred_gmean = np.argmax([gmean_dist2, gmean_dist1])
    
            if pred_gmean == score :
                correct_gmean.append(1) 
            else :
                correct_gmean.append(0)
        
        acc_mean = np.mean(correct_gmean) * 100 
    
        all_acc['Mean'].append(acc_mean)
    
        if acc_mean > 50 :
            dominant = i

    val_acc = []
    for sess_id in couples_data[val_id] : 
        embeddings = couples_data[val_id][sess_id]['embeddings']
        true_score = couples_data[val_id][sess_id]['score']

        X = pca.transform(embeddings)

        gmean = np.mean(X, axis=0)
        gmean_dist1 = euclidean(cent1, gmean)
        gmean_dist2 = euclidean(cent2, gmean)

        if dominant == 0 : 
            pred_gmean = np.argmax([gmean_dist1, gmean_dist2])
        elif dominant == 1 :
            pred_gmean = np.argmax([gmean_dist2, gmean_dist1])


        val_acc.append(pred_gmean == true_score)
    val_acc = np.mean(val_acc)


    ret = {}
    for test_id in test_list : 
        ret[test_id] = {}
        for sess_id in couples_data[test_id] : 
            embeddings = couples_data[test_id][sess_id]['embeddings']
            true_score = couples_data[test_id][sess_id]['score']

            X = pca.transform(embeddings)

            gmean = np.mean(X, axis=0)
            gmean_dist1 = euclidean(cent1, gmean)
            gmean_dist2 = euclidean(cent2, gmean)

            if dominant == 0 : 
                pred_gmean = np.argmax([gmean_dist1, gmean_dist2])
            elif dominant == 1 :
                pred_gmean = np.argmax([gmean_dist2, gmean_dist1])


            ret[test_id][sess_id] = {}
            ret[test_id][sess_id]['score'] = pred_gmean
            ret[test_id][sess_id]['val_acc'] = val_acc
    return ret
    
if __name__ == '__main__':
     
    if len(sys.argv) == 2 : 
        embed_dir = sys.argv[1]
        code = 'negative'
    elif len(sys.argv) == 3 : 
        embed_dir = sys.argv[1]
        code = sys.argv[2]
    elif len(sys.argv) == 4 : 
        embed_dir = '../train_seq2seq/experiments/{}/embeddings/{}_{}'
        embed_dir = embed_dir.format(sys.argv[1], sys.argv[2], sys.argv[3])
        code = 'negative'
    elif len(sys.argv) == 5 : 
        embed_dir = '../train_seq2seq/experiments/{}/embeddings/{}_{}'
        embed_dir = embed_dir.format(sys.argv[1], sys.argv[2], sys.argv[3])
        code = sys.argv[4]

    behaviors = [code]

    lists_mod = './lists_mod/{}.txt'.format(code)

    print(embed_dir)
    
    beh_pos = dict( [(b, 2*i) for i, b in enumerate(behaviors)])
    
    n_couples   = 280
    n_components = 2
    random_restart = 1
    normalize_embeddings = True
    use_moving_average = True
    use_pca = True
    n_pca = 50
    use_lda = False
    shuffle = True
    
    # =============================================================================================================================
    # Load embedding data
    # =============================================================================================================================
    coup_ids = []
    fh = open(lists_mod, 'r')
    lines = fh.readlines()
    if shuffle : 
        random.shuffle(lines)
    couple_list = lines[0:n_couples]
    
    print('Loading couples list')
    for line in lines : 
        cols = line.strip().split(',')
        coup_ids.append(cols[-1].split('.')[0])
    coup_ids = np.unique(sorted(coup_ids))
    
    print('Loading data')
    X = [] 
    trans = []
    couple_sizes = []
    couple_idx = []
    couple_score = []
    neg_couple_idx = []
    pos_couple_idx = []
    wif_idx = []
    hus_idx = []
    cur_offset = 0 
    y = dict( [(b,[]) for b in behaviors])
    
    couples_data = {}
    for line in couple_list : 
        # eg. W,0,1.000,465.26wk.ps.h
        cols = line.strip().split(',')
        spk  = 'spk-{}'.format(cols[0].lower())
        try : 
            neg_score = int(cols[beh_pos['negative']])
            neg_rating= float(cols[beh_pos['negative']+1])
        except :
            neg_score = int(cols[1])
            neg_rating= float(cols[2])
    
        couple_filename = '.'.join([cols[-1], spk, 'txt'])
        couple_filepath = os.path.join(embed_dir,couple_filename)
        couple_id = int(cols[-1].split('.')[0])
        couple_timeframe = cols[-1].split('.')[1]
        sess_id = '.'.join(cols[-1].split('.')[1:] + [spk])
        print('({}) {}'.format(neg_score, couple_filename))
    
        try :
            couple_embed = np.load(couple_filepath.replace('.txt','.npy'))
        except FileNotFoundError :
            print('WARNING: Embedding file not found. Skipping...\n{}'.format(couple_filepath.replace('.txt','.npy')))
            continue
    
        if couple_embed.shape[0] == 0 :
            print('ERROR in embedding file')
            print(couple_filepath)
            continue
        if normalize_embeddings : 
            couple_embed = couple_embed / LA.norm(couple_embed, axis=-1).reshape(couple_embed.shape[0],1)
        if use_moving_average :
            couple_embed = movingAverage(couple_embed)
        couple_size = couple_embed.shape[0]
        if couple_size == 0 : 
            print("\tERROR: Empty file {}".format(couple_filepath))
            sys.exit()
    
        # Store collected data
        if couple_id not in couples_data :
            couples_data[couple_id] = {}
    
        if sess_id not in couples_data[couple_id] : 
            couples_data[couple_id][sess_id] = {}
    
        couples_data[couple_id][sess_id]['embeddings'] = couple_embed.tolist()
        couples_data[couple_id][sess_id]['score'] = neg_score
        couples_data[couple_id][sess_id]['rating'] = neg_rating
    
   
    
    all_couple_ids = list(couples_data.keys())
    ret = {}
    
    p = pp.ProcessPool(4)
    res = p.amap(f_kmeans, [(couples_data,[i]) for i in all_couple_ids])
    for r in res.get() : 
        ret.update(r)
    
    correct = []
    val_acc_all = []
    for coup_id in ret.keys() : 
        val_acc = []
        for sess_id in ret[coup_id] : 
            if len(val_acc) == 0 :
                val_acc_all.append(ret[coup_id][sess_id]['val_acc'])
            true_score = couples_data[coup_id][sess_id]['score']
            pred_score = ret[coup_id][sess_id]['score']
            if true_score == pred_score :
                correct.append(1.)
            else : 
                correct.append(0.)
    
    print('Val accuracy : {}'.format(np.mean(val_acc_all)))
    print('Total accuracy : {}'.format(np.mean(correct)))
    
    fh.close()
    
